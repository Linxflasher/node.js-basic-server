// Импортируем модуль из Node.js
const http = require("http");
const routes = require("./routes");

// Функция будет исполняться при каждом запросе сервера
const server = http.createServer(routes);

server.listen(3000);
