const fs = require("fs");

const requestHandler = (request, response) => {
  const url = request.url;
  const method = request.method;

  if (url === "/") {
    response.write(
      '<html><head><title>First Node.js page</title></head><body><h1>Enter message</h1><form action="/message" method="POST"><input type="text" name="message"/><button type="submit">Send</button></form></body></html>'
    );
    return response.end();
  }

  if (url === "/message" && method === "POST") {
    const body = [];
    request.on("data", chunk => {
      console.log(chunk);

      body.push(chunk);
    });
    return request.on("end", () => {
      const parsedBody = Buffer.concat(body).toString();
      console.log(parsedBody);
      const message = parsedBody.split("=")[1];
      fs.writeFile("message.txt", message, err => {
        response.statusCode = 302;
        response.setHeader("Location", "/");
        return response.end();
      });
    });
  }
  

  // process.exit(); - останавливает сервер
  response.setHeader("Content-Type", "text/html");
  response.write(
    "<html><head><title>First Node .js page</title></head><body><h1>My first Node.js server response in HTML</h1></body></html>"
  );
  response.end();
};

module.exports = requestHandler;
